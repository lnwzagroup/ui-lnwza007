/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.framelnwza.swing1;

import java.awt.Color;
import java.awt.Font;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author Gigabyte
 */
public class Frame1 {

    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(1600, 900);
        
        JLabel lblHelloWorld= new JLabel("Hello World!");
        lblHelloWorld.setBackground(Color.CYAN);
        lblHelloWorld.setOpaque(true);
        lblHelloWorld.setFont(new Font("Verdana",Font.PLAIN, 50));
        frame.add(lblHelloWorld);
        
        frame.setVisible(true);
    }

}
