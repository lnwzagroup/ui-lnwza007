/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.framelnwza.swing1;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

class MyActionListener implements ActionListener{

    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println("MyActionListener:Action");
    }
}
/**
 *
 * @author Gigabyte
 */
public class HelloMe implements ActionListener{

    public static void main(String[] args) {
        JFrame frmMain = new JFrame("Hello Me");
        frmMain.setSize(500, 300);
        frmMain.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JLabel lblYourName = new JLabel("Your Name: ");
        lblYourName.setSize(80, 20);
        lblYourName.setLocation(5, 5);
        lblYourName.setOpaque(true);
        lblYourName.setBackground(Color.CYAN);

        JTextField txtYourName = new JTextField();
        txtYourName.setSize(200, 20);
        txtYourName.setLocation(90, 5);
        
        JButton btn = new JButton("Hello");
        btn.setSize(80, 20);
        btn.setLocation(90, 40);
        MyActionListener Action = new MyActionListener();
        btn.addActionListener(Action);
        btn.addActionListener(new HelloMe());
        
        ActionListener actionlistener = new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Anonymous Class:Action");
            }
        
        };
        btn.addActionListener(actionlistener);
        
        JLabel lblHello = new JLabel("Hello....", JLabel.CENTER);
        lblHello.setSize(200, 20);
        lblHello.setBackground(Color.CYAN);
        lblHello.setLocation(90, 70);

        frmMain.setLayout(null);
        frmMain.add(lblYourName);
        frmMain.add(txtYourName);
        frmMain.add(btn);
        frmMain.add(lblHello);
        
        btn.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                String name = txtYourName.getText();
                lblHello.setText("Hello"+" "+name);
                        
            }
        
        });

        frmMain.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println("Hello Me :Action");
    }

}
